require 'carrierwave/orm/activerecord'

class Micropost < ApplicationRecord
  belongs_to :user
  attr_accessor :avatar
  mount_uploader :avatar, AvatarUploader
  default_scope -> { order(created_at: :desc) }
  validates :user_id, presence: true
  validates :content, presence: true, length: { maximum: 140 }
  validates :rating, presence: true, :numericality => { only_integer: true, :less_than_or_equal_to => 5 }
  validate  :avatar_size

  private

    # アップロードされた画像のサイズをバリデーションする
    def avatar_size
      if avatar.size > 5.megabytes
        errors.add(:avatar, "5MB未満にする必要があります")
      end
    end

end
