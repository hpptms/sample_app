class AvatarUploader < CarrierWave::Uploader::Base
  include Cloudinary::CarrierWave
  include CarrierWave::MiniMagick
  process resize_to_limit: [400, 400]

  # アップロード可能な拡張子のリスト
  def extension_white_list
    %w(jpg jpeg gif png)
  end
  
  version :full do    
    process :convert => 'jpg'
    cloudinary_transformation :quality => 80
  end
  
end
