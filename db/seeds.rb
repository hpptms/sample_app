# ユーザー
User.create!(name:  "カバ夫",
             email: "takanori.tomita.pg@gmail.com",
             password:              "pikatyuu4188",
             password_confirmation: "pikatyuu4188",
             admin:     true,
             activated: true,
             activated_at: Time.zone.now)

# 90.times do |n|
#   name  = Faker::Name.name
#   email = "example-#{n+1}@railstutorial.org"
#   password = "password"
#   User.create!(name:  name,
#                email: email,
#                password:              password,
#                password_confirmation: password,
#                activated: true,
#                activated_at: Time.zone.now)
# end
#
# # マイクロポスト
# users = User.order(:created_at).take(6)
# 50.times do
#   rating = 4
#   content = Faker::Lorem.sentence(5)
#   users.each { |user| user.microposts.create!(content: content) }
# end
#
# # リレーションシップ
# users = User.all
# user  = users.first
# following = users[2..50]
# followers = users[3..40]
# following.each { |followed| user.follow(followed) }
# followers.each { |follower| follower.follow(user) }
