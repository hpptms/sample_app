class AddRatingToMicroposts < ActiveRecord::Migration[5.0]
  def change
    add_column :microposts, :rating, :integer, limit: 5
  end
end
